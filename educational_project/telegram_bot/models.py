from django.db import models


class TelegramUser(models.Model):
    telegram_id = models.PositiveIntegerField(verbose_name="Телеграмм id", null=True)
    nickname = models.CharField(max_length=255, verbose_name="Никнейм в телеграмме")
    contact = models.CharField(max_length=11, verbose_name="Телефон")
    full_name = models.CharField(max_length=50, verbose_name="Полное имя")
    premium = models.CharField(max_length=15, verbose_name="Премиум аккаунт", null=True)

    def __str__(self):
        if self.nickname:
            return self.nickname
        else:
            return self.full_name
