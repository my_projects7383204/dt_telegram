from telegram import Bot
from telegram.utils.request import Request

from config import TOKEN


request = Request(
    connect_timeout=0.5,
    read_timeout=1.0,
)

bot = Bot(
    request=request,
    token=TOKEN,
)
