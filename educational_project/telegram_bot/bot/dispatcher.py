from telegram.ext import Dispatcher, CommandHandler, MessageHandler, Filters

from .handlers.commands import command_start, set_phone, reply_to_get_contact, filter_text, get_user_info
from .bot import bot


def addition_commands_to_dispatcher(dispatcher: Dispatcher):
    # Start command
    dispatcher.add_handler(CommandHandler("start", command_start))

    # Addition user phone to db
    dispatcher.add_handler(CommandHandler("set_phone", set_phone))

    # get user info
    dispatcher.add_handler(CommandHandler("me", get_user_info))

    # response to get user contact
    dispatcher.add_handler(MessageHandler(Filters.contact, reply_to_get_contact))

    # processing non-working text
    dispatcher.add_handler(MessageHandler(Filters.text, filter_text))

    return dispatcher


dispatcher_command = addition_commands_to_dispatcher(Dispatcher(bot, update_queue=None))

