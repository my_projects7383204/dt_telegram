from telegram import Update, ReplyKeyboardRemove
from telegram.ext import CallbackContext

from config import HOST
from .buttons import get_phone_reply_keyboard
from .untils import blocking_command_without_phone_number
from .static_text import (START_TEXT, PHONE_TEXT,
                          THANKS_TEXT, ERROR_TEXT,
                          USER_INFO_TELEGRAM,
                          BOOL_INFO)
from ...models import TelegramUser


def command_start(update: Update, context: CallbackContext):
    nickname = BOOL_INFO.get(update.effective_user.username,
                             update.effective_user.username)

    TelegramUser.objects.get_or_create(
        telegram_id=update.effective_user.id,
        nickname=nickname,
        full_name=update.effective_user.full_name,
        premium=update.effective_user.is_premium
    )

    update.message.reply_html(text=START_TEXT)


def set_phone(update: Update, context: CallbackContext):
    update.message.reply_text(text=PHONE_TEXT,
                              reply_markup=get_phone_reply_keyboard()
                              )


@blocking_command_without_phone_number
def get_user_info(update: Update, context: CallbackContext):
    user = TelegramUser.objects.get(telegram_id=update.effective_user.id)
    info_text = USER_INFO_TELEGRAM.format(user.nickname,
                                          user.full_name,
                                          user.contact,
                                          BOOL_INFO.get(user.premium, "Имеется"),
                                          user.telegram_id
                                          )
    update.message.reply_html(
        text=f"{info_text}"
             f"https://{HOST}/me/?user_id={user.telegram_id}")


def reply_to_get_contact(update: Update, context: CallbackContext):
    TelegramUser.objects.filter(
        telegram_id=update.effective_user.id).update(
        contact=update.message.contact.phone_number)

    update.message.reply_text(text=THANKS_TEXT,
                              reply_markup=ReplyKeyboardRemove()
                              )


def filter_text(update: Update, context: CallbackContext):
    update.message.reply_text(text=ERROR_TEXT)
