from telegram import KeyboardButton, ReplyKeyboardMarkup
from .static_text import BUTTON_PHONE


def get_phone_reply_keyboard():
    keyboad = [
        [
            KeyboardButton(BUTTON_PHONE, request_contact=True)
        ]
    ]
    return ReplyKeyboardMarkup(
        keyboard=keyboad,
        resize_keyboard=True,
    )
