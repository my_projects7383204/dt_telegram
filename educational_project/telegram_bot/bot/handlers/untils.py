from .static_text import NOT_PHONE
from ...models import TelegramUser


def blocking_command_without_phone_number(func):
    def wrapper(update, context):
        telegram_id = update.effective_user.id
        users = TelegramUser.objects.get(telegram_id=telegram_id)
        if users.contact:
            res = func(update, context)
        else:
            res = update.message.reply_text(text=NOT_PHONE)
        return res

    return wrapper
