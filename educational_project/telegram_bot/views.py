import json

from django.views import View
from django.http import JsonResponse, HttpResponse
from telegram import Update

from .models import TelegramUser
from .bot.handlers.static_text import USER_INFO_HTTP, BOOL_INFO
from .bot.dispatcher import dispatcher_command
from .bot.bot import bot


class TelegramWebHook(View):
    def post(self, request):
        user_reply = json.loads(request.body)
        update = Update.de_json(user_reply, bot)
        dispatcher_command.process_update(update)

        return JsonResponse({"ok": "POST request processed"})


class HttpMe(View):
    def get(self, request):
        user_id = request.GET.get("user_id")
        user = TelegramUser.objects.get(telegram_id=user_id)

        response = USER_INFO_HTTP.format(
            user.nickname,
            user.full_name,
            user.contact,
            BOOL_INFO.get(user.premium, "Имеется"),
            user.telegram_id
        )

        return HttpResponse(response)
