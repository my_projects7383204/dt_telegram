from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from config import TOKEN
from .views import TelegramWebHook, HttpMe

urlpatterns = [
    path(f"{TOKEN}/", csrf_exempt(TelegramWebHook.as_view()), name="webhook"),
    path("me/", HttpMe.as_view(), name="me")
]
