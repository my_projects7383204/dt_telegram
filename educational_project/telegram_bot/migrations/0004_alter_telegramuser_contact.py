# Generated by Django 4.0 on 2024-01-25 09:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('telegram_bot', '0003_alter_telegramuser_chat_id_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='telegramuser',
            name='contact',
            field=models.CharField(max_length=11, verbose_name='Телефон'),
        ),
    ]
